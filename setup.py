from subprocess import call
from setuptools import setup, find_packages
from setuptools.command.install import install as _install
from distutils.command.build import build as _build
from rts2view import __version__

class Build(_build):
    """Builds interfaces"""
    def run(self):
        _build.run(self)

        def generate_interface(path):
            call(['/bin/sh', 'generate_interface.sh'], cwd=path)

        self.execute(generate_interface, ['./bin'], msg='Generating Thrift interface')
            
setup(
    name='RTS2View',
    description='RTS2 View',
    version=__version__,
    author='Carel van Gend',
    author_email='carel@saao.ac.za',
    # TODO: license='',
    packages=find_packages(),
    install_requires=['thrift', 'future', 'astropy>=1.0', 'paramiko',],
    cmdclass={
        'build': Build,
    },
    data_files=[
        ('/etc/init/', ['etc/init/rts2view.conf']),
	('/etc/logrotate.d/', ['etc/logrotate.d/rts2view']),
        ('/usr/local/etc/', ['etc/rts2view.cfg'])
    ],
    entry_points={
        'console_scripts': [
            'rts2view = rts2view.server.rts2view_server:run',
            'rts2view-cli = rts2view.cli.rts2view_cli:main',
        ],
    }
)
