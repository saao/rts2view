#!/bin/bash
echo "In generate_interface.sh..."
echo "Removing old interface directory"
rm -rf ../rts2view/rts2view_interface
echo "Creating new interface directory"
mkdir ../rts2view/rts2view_interface
touch ../rts2view/rts2view_interface/__init__.py
echo "Generating interface code"
thrift -out ../rts2view --gen py:new_style ../etc/rts2view_interface.thrift
