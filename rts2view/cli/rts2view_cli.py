#!/usr/bin/env python
"""RTS2 View command line client."""

from rts2view.client.rts2view_client import RTS2ViewClient
import cmd
import argparse
import sys
from future.builtins import input

from rts2view import __version__

class CLI(cmd.Cmd):
    """RTS2View command line client.

    Commands:
    
    mountinfo
    derotator
    slew
    park
    quit

    For help for a specific command type 'help <command>'.
    """

    prompt = "RTS2 View >> "

    def __init__(self, client):
        cmd.Cmd.__init__(self)
        self.client = client

    def default(self, line):
        command = line.split()[0].lower()
        if command == 'm':
            self.do_infomount()
        elif command == 'd':
            self.do_infoderotator()
        elif command == 's':
            self.do_slew()
        elif command == 'p':
            self.do_park()
        elif command == 'q':
            return True
        else:
            cmd.Cmd.default(self, line)

    def emptyline(self):
        """Do nothing when an empty line is entered.

        The default behaviour is to repeat the last command and that's not what
        we want.
        """
        pass

    def do_help(self, line):
        if not line:
            print(self.__doc__)
        else:
            cmd.Cmd.do_help(self, line)

    def do_quit(self, line):
        """Exit the program."""
        return True

    def do_EOF(self, line):
        return True

    def do_mountinfo(self, line=None):
        '''Show the RTS2 mount information'''
        print(self.client.infoMount())

    def do_derotatorinfo(self, line=None):
        '''Show the RTS2 derotator information'''
        print(self.client.infoDerotator())

    def do_slew(self, line=None):
        '''Slew to a specified target'''
        radec = RaDec()
        ra = raw_input("Please enter the target ra: ")
        dec = raw_input("Please enter the target dec: ")
        radec = RaDec(ra, dec)
        print("CLI requesting slew to RA = {}, DEC = ".format(radec.ra, radec.dec))
        self.client.Slew(target_info)

    def do_park(self):
        '''Park the telescope'''
        self.client.Park()


def main():
    parser = argparse.ArgumentParser(description=__doc__,
        prog='rts2view-cli')

    parser.add_argument('--host', default='localhost',
                        help="The remote host name")
    parser.add_argument('--port', default=9093,
                        help="The port on the remote host")

    arguments = parser.parse_args()

    client = RTS2ViewClient(arguments.host, arguments.port)

    intro = 'RTS2 View {}\nType "help" for more information.'.format(__version__)

    try:
        CLI(client).cmdloop(intro)
    except KeyboardInterrupt:
        sys.exit(0)


if __name__ == '__main__':
    main()
