import logging as log

from thrift import Thrift
from thrift.transport import TSocket, TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TCompactProtocol

from rts2view.rts2view_interface.ObservatoryService import Client

class RTS2ViewClient:
    def __init__(self, tcp_host='1ms1.suth.saao.ac.za', tcp_port=9093):
        self.tcp_host = tcp_host
        self.tcp_port = tcp_port
        self.socket = TSocket.TSocket(tcp_host, tcp_port)
        self.socket.setTimeout(20000)
        self.transport = TTransport.TBufferedTransport(self.socket)
        self.protocol = TBinaryProtocol.TBinaryProtocol(self.transport)
        self.client = Client(self.protocol)
        self.transport.open()

    def infoMount(self):
        print("Client calling remote server infoMount")
        return self.client.infoMount()

    def infoDerotator(self):
        return self.client.infoDerotator()

    def Slew(self, target_radec):
        print("Client requesting slew to {}".format(target_radec))
        pass # For now
        return self.client.Slew(target_radec)

    def Park(self):
        pass # For now
